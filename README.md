## Database
For your convenience, this application is using an in-memory H2 database, so simply run the app from IntelliJ. 
H2 console is available at 
`localhost:8080/h2-console`. In order to connect, you need three pieces of information:

* Address - will be displayed in application logs during startup, e.g.  
`H2 console available at '/h2-console'. Database available at 'jdbc:h2:mem:f36cbe4d-4d59-4dc0-a721-f1020d0fdf15'`
* Username - `sa` (default)
* Password - no password (default)


## API
This project contains OpenAPI dependencies, this means that you can access the exposed API at:
 
* http://localhost:8080/swagger-ui.html - for testing endpoints
* http://localhost:8080/v3/api-docs/ - for descriptors

I also leave you with a TestController, which allows you to quickly create a few users and tasks. 
`localhost:8080/generate`

## Testing
The project also contains examples of tests:
 
* unit tests pertaining to validators, factories
* integration tests for:
    * creating/getting/deleting users
    * assigning task to user, starting/stopping progress
    
Those tests are just examples, but coverage is still >50%.

## Project details
The task is quite permissive with regard to implementation, but below are some additional details of my solution.

* Choosing a person to autoassign a task to is done in a rather obvious way - the person that has the least amount of 
points will be selected. In the beginning, I was considering additional conditions like current number of tasks, but 
it could be misleading - a person with one 21-pts task does more than person with 10 2-pointers (save that 1 point).

* assignedPoints is the sum of all OPEN and IN_PROGRESS tasks. However, when considering the forced task reassignment 
(feature), only OPEN tasks are taken into account. This is correct, because IN_PROGRESS tasks are resistant to forced 
changes (the user is potentially working on them, they shouldn't be touched). This also means that the feature that 
forces task reassignment can end with a failure in case the selected user does not have enough open tasks to reassign to 
other people. How could this happen? When a person has many assigned tasks, but all of them are in progress, there will
be not enough open tasks to shuffle around.

* When you try to forcibly assign a person with a task, and they do not have required points to do this, their OPEN 
tasks will get reassigned to others (ordered by task points ascending, so that simple tasks go first). It may happen 
that other users do not have enough points to accept those additional tasks. In this case, the task will get detached 
from the current user and left without assignee. You can resolve that situation manually, later.

* The number of points a person can accept is constant in this assignment (40 pts), but I let it be changed in 
`application.properties`. It can be whatever, but please note that new tasks you create can never have more points than
that number. Also, task points are validated against Fibonacci sequence - the mathematical one (...8, 13, 21, 34...), 
and not the scrum-poker one (...8, 13, 20, 40...).

## Contact
In case you had any questions, please write me @ damianpi.contact@gmail.com