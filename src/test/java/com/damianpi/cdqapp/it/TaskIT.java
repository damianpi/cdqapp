package com.damianpi.cdqapp.it;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.TaskRequest;
import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.TaskRepository;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TaskIT {

    private static final String UUID_PATTERN = "\"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\"";

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void shouldCorrectlyCreateAndAssignTasks() throws Exception {
        // Should throw exception if there are no users yet
        TaskRequest createTask = new TaskRequest();
        createTask.points = 5;
        createTask.title = "taskTitle";
        createTask.description = "taskDescription";

        MvcResult createTaskNoUserResult = mockMvc.perform(MockMvcRequestBuilders.post("/task")
                                                  .contentType(MediaType.APPLICATION_JSON)
                                                  .content(objectMapper.writeValueAsString(createTask))).andReturn();

        assertThat(createTaskNoUserResult.getResponse().getStatus()).isEqualTo(400);
        assertThat(createTaskNoUserResult.getResponse()
                                         .getContentAsString()).isEqualTo("Cannot find any user to autoassign");


        // Create one user and assign him a task
        PersonRequest createPersonRequest = new PersonRequest();
        createPersonRequest.username = "user1";
        createPersonRequest.name = "user1Name";
        createPersonRequest.surname = "user1Surname";
        createPersonRequest.position = "user1Position";
        MvcResult createUserResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .content(objectMapper.writeValueAsString(createPersonRequest))).andReturn();

        assertThat(createUserResult.getResponse().getStatus()).isEqualTo(200);
        assertThat(createUserResult.getResponse().getContentAsString()).isEqualTo(createPersonRequest.username);

        MvcResult createTaskUserExistsResult = mockMvc.perform(MockMvcRequestBuilders.post("/task")
                                                     .contentType(MediaType.APPLICATION_JSON)
                                                     .content(objectMapper.writeValueAsString(createTask))).andReturn();

        assertThat(createTaskUserExistsResult.getResponse().getStatus()).isEqualTo(200);
        assertThat(createTaskUserExistsResult.getResponse().getContentAsString()).matches(UUID_PATTERN);

        String trimmedUuid = createTaskUserExistsResult.getResponse().getContentAsString().replace("\"", "");

        UUID taskUuid = UUID.fromString(trimmedUuid);
        Task assignedTask = taskRepository.findById(taskUuid).orElseThrow();

        assertThat(assignedTask).isNotNull();
        assertThat(assignedTask.getPoints()).isEqualTo(createTask.points);
        assertThat(assignedTask.getTitle()).isEqualTo(createTask.title);
        assertThat(assignedTask.getDescription()).isEqualTo(createTask.description);
        assertThat(assignedTask.getTaskStatus()).isEqualByComparingTo(Task.TaskStatus.OPEN);

        Person person = personRepository.findByUsername(createPersonRequest.username);
        assertThat(person.getTasks().size()).isEqualTo(1);
        assertThat(person.getAssignedPoints()).isEqualTo(createTask.points);


        // Start progress on a task
        MvcResult startProgressResult = mockMvc.perform(MockMvcRequestBuilders.put("/task/start/" + taskUuid.toString())
                                               .contentType(MediaType.APPLICATION_JSON)).andReturn();
        Task inProgressTask = taskRepository.findById(taskUuid).orElseThrow();
        assertThat(startProgressResult.getResponse().getStatus()).isEqualTo(200);
        assertThat(inProgressTask.getTaskStatus()).isEqualByComparingTo(Task.TaskStatus.IN_PROGRESS);


        // Stop progress on a task
        MvcResult stopProgressResult = mockMvc.perform(MockMvcRequestBuilders.put("/task/stop/" + taskUuid.toString())
                                              .contentType(MediaType.APPLICATION_JSON)).andReturn();
        Task stoppedTask = taskRepository.findById(taskUuid).orElseThrow();
        assertThat(stopProgressResult.getResponse().getStatus()).isEqualTo(200);
        assertThat(stoppedTask.getTaskStatus()).isEqualByComparingTo(Task.TaskStatus.CLOSED);

        Person stoppedTaskPerson = personRepository.findByUsername(createPersonRequest.username);
        assertThat(stoppedTaskPerson.getAssignedPoints()).isEqualTo(0);

    }
}
