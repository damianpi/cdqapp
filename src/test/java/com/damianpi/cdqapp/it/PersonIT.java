package com.damianpi.cdqapp.it;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.PersonResponse;
import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.entity.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.*;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class PersonIT {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void shouldCorrectlyCreateAndDeleteUser() throws Exception {
        // Create new user
        PersonRequest createRequest = new PersonRequest();
        createRequest.username = "user1";
        createRequest.name = "user1Name";
        createRequest.surname = "user1Surname";
        createRequest.position = "user1Position";
        MvcResult createResult = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createRequest))).andReturn();

        assertThat(createResult.getResponse().getStatus()).isEqualTo(200);
        assertThat(createResult.getResponse().getContentAsString()).isEqualTo(createRequest.username);

        Person person = personRepository.findByUsername(createRequest.username);
        assertThat(person.getAssignedPoints()).isEqualTo(0);
        assertThat(person.getPersonDetails().getName()).isEqualTo(createRequest.name);
        assertThat(person.getPersonDetails().getSurname()).isEqualTo(createRequest.surname);
        assertThat(person.getPersonDetails().getPosition()).isEqualTo(createRequest.position);


        // Get existing user
        MvcResult getResult = mockMvc.perform(MockMvcRequestBuilders.get("/user/user1")).andReturn();
        PersonResponse getResponse =
                objectMapper.readValue(getResult.getResponse().getContentAsString(), PersonResponse.class);

        assertThat(getResponse.username).isEqualTo("user1");
        assertThat(getResponse.name).isEqualTo("user1Name");
        assertThat(getResponse.surname).isEqualTo("user1Surname");
        assertThat(getResponse.position).isEqualTo("user1Position");


        // Delete existing user
        Person beforeDelete = personRepository.findByUsername("user1");
        assertThat(beforeDelete).isNotNull();

        MvcResult deleteResult = mockMvc.perform(MockMvcRequestBuilders.delete("/user/user1")).andReturn();
        assertThat(deleteResult.getResponse().getStatus()).isEqualTo(200);

        Person afterDelete = personRepository.findByUsername("user1");
        assertThat(afterDelete).isNull();
    }
}
