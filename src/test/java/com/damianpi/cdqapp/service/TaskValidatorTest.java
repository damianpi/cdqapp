package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.TaskRequest;
import com.damianpi.cdqapp.controller.dto.UpdateTaskRequest;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import com.damianpi.cdqapp.service.exception.ValidationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class TaskValidatorTest {

    static TaskValidator validator;

    @BeforeAll
    public static void beforeAll() {
        validator = new TaskValidator(40);
    }

    @Test
    public void shouldThrowExceptionOnBadStatusType() {
        Throwable thrown = catchThrowable(() -> validator.validateTaskStatus("foo"));

        assertThat(thrown).isInstanceOf(ValidationException.class).hasMessageContaining("No such task status");
    }

    @Test
    public void shouldValidateCorrectStatusType() {
        Throwable thrown = catchThrowable(() -> validator.validateTaskStatus("OPEN"));

        assertThat(thrown).isNull();
    }

    @Test
    public void shouldThrowExceptionWhenUserDoesNotHaveEnoughPointsToAcceptTask() {
        Person person = new Person();
        person.setAssignedPoints(35);
        Task task = new Task();
        task.setPoints(13);

        Throwable thrown = catchThrowable(() -> validator.canAssignTask(person, task));

        assertThat(thrown).isInstanceOf(ValidationException.class)
                .hasMessageContaining("User cannot accept this many points");
    }

    @Test
    public void shouldNotValidateNewTaskThatHasMorePointsThanAllowed() {
        TaskRequest request = new TaskRequest();
        request.points = 100;

        Throwable thrown = catchThrowable(() -> validator.validateNewTask(request));

        assertThat(thrown).isInstanceOf(ValidationException.class)
                .hasMessageContaining("It is not allowed to have task points higher than 40");
    }

    @Test
    public void shouldNotValidateNewTaskWhenPointsDoNotBelongToFibonacci() {
        TaskRequest request = new TaskRequest();
        request.points = 40;

        Throwable thrown = catchThrowable(() -> validator.validateNewTask(request));

        assertThat(thrown).isInstanceOf(ValidationException.class)
                .hasMessageContaining("Number of points must belong to Fibonacci sequence");
    }

    @Test
    public void shouldValidateCorrectNewTask() {
        TaskRequest request = new TaskRequest();
        request.points = 21;

        Throwable thrown = catchThrowable(() -> validator.validateNewTask(request));

        assertThat(thrown).isNull();
    }

    @Test
    public void shouldNotValidateUpdateIfNewPointsAreMoreThanUserAllowed() {
        UpdateTaskRequest request = new UpdateTaskRequest();
        request.points = 34;
        Person person = new Person();
        person.setAssignedPoints(34);
        Task task = new Task();
        task.setPoints(5);
        task.setAssignee(person);

        Throwable thrown = catchThrowable(() -> validator.validateUpdateTask(task, request));

        assertThat(thrown).isInstanceOf(ValidationException.class)
                .hasMessageContaining("The new number of points would exceed the user limit of 40");
    }

    @Test
    public void shouldNotValidateUpdateIfNewPointsAreNotFibonacci() {
        UpdateTaskRequest request = new UpdateTaskRequest();
        request.points = 35;
        Person person = new Person();
        person.setAssignedPoints(5);
        Task task = new Task();
        task.setPoints(5);
        task.setAssignee(person);

        Throwable thrown = catchThrowable(() -> validator.validateUpdateTask(task, request));

        assertThat(thrown).isInstanceOf(ValidationException.class)
                .hasMessageContaining("Number of points must belong to Fibonacci sequence");
    }

    @Test
    public void shouldCorrectlyResolveNumberOfRequiredPointsToFree() {
        Person person = new Person();
        person.setAssignedPoints(38);
        Task task = new Task();
        task.setPoints(5);

        int pointsToFree = validator.resolveRequiredAssignedPoints(person, task);
        assertThat(pointsToFree).isEqualTo(3);
    }
}
