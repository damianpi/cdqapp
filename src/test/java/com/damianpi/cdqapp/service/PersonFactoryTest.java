package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.PersonResponse;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.PersonDetails;
import com.damianpi.cdqapp.repository.entity.Task;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class PersonFactoryTest {

    @Test
    public void shouldCreatePersonFromRequest() {
        PersonRequest request = new PersonRequest();
        request.username = "user1";
        request.name = "user1Name";
        request.surname = "user1Surname";
        request.position = "user1Position";

        Person person = PersonFactory.of(request);

        assertThat(person.getUsername()).isEqualTo(request.username);
        assertThat(person.getPersonDetails().getName()).isEqualTo(request.name);
        assertThat(person.getPersonDetails().getSurname()).isEqualTo(request.surname);
        assertThat(person.getPersonDetails().getPosition()).isEqualTo(request.position);
        assertThat(person.getTasks().size()).isEqualTo(0);
        assertThat(person.getAssignedPoints()).isEqualTo(0);
    }

    @Test
    public void shouldCreatePersonResponseFromPerson() {
        Person person = new Person();
        person.setAssignedPoints(5);
        person.setUsername("user1");

        PersonDetails details = new PersonDetails();
        details.setName("user1Name");
        person.setPersonDetails(details);

        Task activeTask = new Task();
        activeTask.setPoints(5);
        activeTask.setTaskStatus(Task.TaskStatus.OPEN);

        Task closedTask = new Task();
        closedTask.setPoints(3);
        closedTask.setTaskStatus(Task.TaskStatus.CLOSED);

        person.setTasks(Lists.list(activeTask, closedTask));

        PersonResponse response = PersonFactory.of(person);

        assertThat(response.username).isEqualTo("user1");
        assertThat(response.name).isEqualTo("user1Name");
        assertThat(response.tasks.size()).isEqualTo(1);
        assertThat(response.tasks.get(0).taskStatus).isEqualTo("OPEN");
    }
}
