package com.damianpi.cdqapp.controller.dto;

import javax.validation.constraints.NotNull;

public class PersonRequest {

    @NotNull
    public String username;
    public String name = "";
    public String surname = "";
    public String position = "";
}
