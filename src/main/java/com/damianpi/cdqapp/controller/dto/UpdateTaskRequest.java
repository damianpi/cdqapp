package com.damianpi.cdqapp.controller.dto;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class UpdateTaskRequest {
    @NotNull
    public UUID uuid;
    public Integer points;
    public String title = "";
    public String description = "";
}
