package com.damianpi.cdqapp.controller.dto;

import java.util.List;

public class PersonResponse {
    public String username;
    public String name;
    public String surname;
    public String position;
    public List<TaskSummaryDTO> tasks;
}

