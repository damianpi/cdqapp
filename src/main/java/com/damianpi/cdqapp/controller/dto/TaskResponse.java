package com.damianpi.cdqapp.controller.dto;

import java.util.UUID;

public class TaskResponse {
    public UUID uuid;
    public Integer points;
    public String taskStatus;
    public String title;
    public String description;
    public String assignee;
}
