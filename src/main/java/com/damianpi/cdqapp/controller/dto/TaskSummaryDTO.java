package com.damianpi.cdqapp.controller.dto;

import java.util.UUID;

public class TaskSummaryDTO {
    public UUID uuid;
    public String title;
    public Integer points;
    public String taskStatus;
    public String assignee;
}
