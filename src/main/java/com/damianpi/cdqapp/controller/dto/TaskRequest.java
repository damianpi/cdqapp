package com.damianpi.cdqapp.controller.dto;

public class TaskRequest {

    public Integer points = 0;
    public String title = "";
    public String description = "";
}
