package com.damianpi.cdqapp.controller;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.PersonResponse;
import com.damianpi.cdqapp.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("{username}")
    public ResponseEntity<PersonResponse> getUser(@PathVariable String username) {
        return ResponseEntity.ok(personService.getPerson(username));
    }

/*
{
  "username": "user1",
  "name": "user1",
  "surname": "user1",
  "position": "user1"
}
*/
    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody PersonRequest request) {
        return ResponseEntity.ok(personService.createPerson(request));
    }

    @DeleteMapping("{username}")
    public ResponseEntity<Void> deleteUser(@PathVariable String username) {
        personService.deletePerson(username);
        return ResponseEntity.ok().build();
    }
}
