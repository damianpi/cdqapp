package com.damianpi.cdqapp.controller;

import com.damianpi.cdqapp.service.exception.PersonException;
import com.damianpi.cdqapp.service.exception.TaskException;
import com.damianpi.cdqapp.service.exception.ValidationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {PersonException.class,
            TaskException.class,
            ValidationException.class,
            NullPointerException.class})
    public ResponseEntity<Object> handle(RuntimeException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

}
