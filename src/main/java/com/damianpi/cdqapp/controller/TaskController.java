package com.damianpi.cdqapp.controller;

import com.damianpi.cdqapp.controller.dto.*;
import com.damianpi.cdqapp.service.TaskAssignmentService;
import com.damianpi.cdqapp.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("task")
public class TaskController {

    private final TaskService taskService;
    private final TaskAssignmentService taskAssignmentService;

    public TaskController(TaskService taskService, TaskAssignmentService taskAssignmentService) {
        this.taskService = taskService;
        this.taskAssignmentService = taskAssignmentService;
    }

    @PostMapping
    public UUID createTask(@RequestBody TaskRequest taskRequest) {
        return taskService.createTask(taskRequest);
    }

    @GetMapping("{uuid}")
    public ResponseEntity<TaskResponse> getTask(@PathVariable UUID uuid) {
        return ResponseEntity.ok(taskService.getTask(uuid));
    }

    @PutMapping
    public ResponseEntity<Void> updateTask(@RequestBody UpdateTaskRequest updateTaskRequest) {
        taskService.updateTask(updateTaskRequest);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{uuid}")
    public ResponseEntity<Void> deleteTask(@PathVariable UUID uuid) {
        taskService.deleteTask(uuid);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/assign/{username}/{taskUuid}")
    public ResponseEntity<Void> assignPersonWithTask(@PathVariable String username, @PathVariable UUID taskUuid) {
        taskAssignmentService.assignTaskWithPerson(username, taskUuid, false);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/assign/{username}/{taskUuid}/force")
    public ResponseEntity<Void> forceAssignPersonWithTask(@PathVariable String username, @PathVariable UUID taskUuid) {
        taskAssignmentService.assignTaskWithPerson(username, taskUuid, true);
        return ResponseEntity.ok().build();
    }

    @GetMapping("list/{username}/{taskStatus}")
    public ResponseEntity<List<TaskSummaryDTO>> getTasksForPerson(@PathVariable String username, @PathVariable String taskStatus) {
        return ResponseEntity.ok(taskService.listTasks(username, taskStatus));
    }

    @GetMapping("list/assigned")
    public ResponseEntity<List<TaskSummaryDTO>> getAllAssignedTasks() {
        return ResponseEntity.ok(taskService.listAssignedTasks());
    }

    @PutMapping("start/{uuid}")
    public ResponseEntity<Void> startProgress(@PathVariable UUID uuid) {
        taskService.startProgress(uuid);
        return ResponseEntity.ok().build();
    }

    @PutMapping("stop/{uuid}")
    public ResponseEntity<Void> stopProgress(@PathVariable UUID uuid) {
        taskService.stopProgress(uuid);
        return ResponseEntity.ok().build();
    }
}
