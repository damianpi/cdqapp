package com.damianpi.cdqapp.controller;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.TaskRequest;
import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.TaskRepository;
import com.damianpi.cdqapp.service.PersonService;
import com.damianpi.cdqapp.service.TaskAssignmentService;
import com.damianpi.cdqapp.service.TaskService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class TestController {

    private final PersonService personService;
    private final TaskService taskService;
    private final TaskAssignmentService taskAssignmentService;
    private final PersonRepository personRepository;
    private final TaskRepository taskRepository;

    public TestController(PersonService personService,
                          TaskService taskService,
                          TaskAssignmentService taskAssignmentService,
                          PersonRepository personRepository,
                          TaskRepository taskRepository) {
        this.personService = personService;
        this.taskService = taskService;
        this.taskAssignmentService = taskAssignmentService;
        this.personRepository = personRepository;
        this.taskRepository = taskRepository;
    }

    // localhost:8080/generate
    @GetMapping("/generate")
    public void generateData() {
        PersonRequest pr1 = new PersonRequest();
        pr1.username = "user1";
        personService.createPerson(pr1);

        PersonRequest pr2 = new PersonRequest();
        pr2.username = "user2";
        personService.createPerson(pr2);

//        PersonRequest pr3 = new PersonRequest();
//        pr3.username = "user3";
//        personService.createPerson(pr3);

        TaskRequest tr1 = new TaskRequest();
        tr1.title = "task1";
        tr1.description = "task1";
        tr1.points = 8;
        UUID task1 = taskService.createTask(tr1);

        TaskRequest tr2 = new TaskRequest();
        tr2.title = "task2";
        tr2.description = "task2";
        tr2.points = 5;
        UUID task2 = taskService.createTask(tr2);

        TaskRequest tr3 = new TaskRequest();
        tr3.title = "task3";
        tr3.description = "task3";
        tr3.points = 5;
        UUID task3 = taskService.createTask(tr3);

        TaskRequest tr4 = new TaskRequest();
        tr4.title = "task4";
        tr4.description = "task4";
        tr4.points = 3;
        UUID task4 = taskService.createTask(tr4);

        TaskRequest tr5 = new TaskRequest();
        tr5.title = "task5";
        tr5.description = "task5";
        tr5.points = 2;
        UUID task5 = taskService.createTask(tr5);

        TaskRequest tr6 = new TaskRequest();
        tr6.title = "task6";
        tr6.description = "task6";
        tr6.points = 1;
        UUID task6 = taskService.createTask(tr6);

        TaskRequest tr7 = new TaskRequest();
        tr7.title = "task7";
        tr7.description = "task7";
        tr7.points = 13;
        UUID task7 = taskService.createTask(tr7);

        TaskRequest tr8 = new TaskRequest();
        tr8.title = "task8";
        tr8.description = "task8";
        tr8.points = 2;
        UUID task8 = taskService.createTask(tr8);

        TaskRequest tr9 = new TaskRequest();
        tr9.title = "task9";
        tr9.description = "task9";
        tr9.points = 21;
        UUID task9 = taskService.createTask(tr9);

        TaskRequest tr10 = new TaskRequest();
        tr10.title = "task10";
        tr10.description = "task10";
        tr10.points = 13;
        UUID task10 = taskService.createTask(tr10);

        TaskRequest tr11 = new TaskRequest();
        tr11.title = "task11";
        tr11.description = "task11";
        tr11.points = 13;
        UUID task11 = taskService.createTask(tr11);
    }

}
