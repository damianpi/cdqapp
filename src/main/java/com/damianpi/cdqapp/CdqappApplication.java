package com.damianpi.cdqapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class CdqappApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdqappApplication.class, args);
	}

}
