package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.TaskRequest;
import com.damianpi.cdqapp.controller.dto.TaskResponse;
import com.damianpi.cdqapp.controller.dto.UpdateTaskRequest;
import com.damianpi.cdqapp.repository.entity.Task;

public class TaskFactory {

    public static Task of(TaskRequest request) {
        Task task = new Task();
        task.setTitle(request.title);
        task.setDescription(request.description);
        task.setPoints(request.points);
        task.setTaskStatus(Task.TaskStatus.OPEN);
        return task;
    }

    public static TaskResponse of(Task task) {
        TaskResponse response = new TaskResponse();
        response.uuid = task.getUuid();
        response.title = task.getTitle();
        response.description = task.getDescription();
        response.taskStatus = task.getTaskStatus().toString();
        response.points = task.getPoints();
        response.assignee = task.getAssignee().getUsername();
        return response;
    }

    public static void update(Task task, UpdateTaskRequest request) {
        task.setTitle(request.title);
        task.setDescription(request.description);
        task.setPoints(request.points);
    }
}
