package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.PersonResponse;
import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.entity.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public String createPerson(PersonRequest request) {
        return personRepository.save(PersonFactory.of(request)).getUsername();
    }

    public PersonResponse getPerson(String username) {
        return PersonFactory.of(personRepository.findByUsername(username));
    }

    @Transactional
    public void deletePerson(String username) {
        Person person = personRepository.findByUsername(username);
        person.getTasks().forEach(t -> t.setAssignee(null));
        personRepository.deleteByUsername(username);
    }
}
