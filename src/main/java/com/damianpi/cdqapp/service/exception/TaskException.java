package com.damianpi.cdqapp.service.exception;

public class TaskException extends RuntimeException {

    public TaskException(String message) {
        super(message);
    }
}
