package com.damianpi.cdqapp.service.exception;

public class PersonException extends RuntimeException {

    public PersonException(String message) {
        super(message);
    }
}
