package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.TaskRequest;
import com.damianpi.cdqapp.controller.dto.UpdateTaskRequest;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import com.damianpi.cdqapp.service.exception.ValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TaskValidator {

    private final int userMaxAssignedPoints;

    public TaskValidator(@Value("${com.damianpi.cdqapp.user-max-points}") int userMaxAssignedPoints) {
        this.userMaxAssignedPoints = userMaxAssignedPoints;
    }

    public void validateNewTask(TaskRequest request) {
        if (request.points > userMaxAssignedPoints) {
            throw new ValidationException("It is not allowed to have task points higher than "
                    + userMaxAssignedPoints);
        }
        if (!doesBelongToFibonacciSequence(request.points)) {
            throw new ValidationException("Number of points must belong to Fibonacci sequence");
        }
    }

    private boolean doesBelongToFibonacciSequence(int number) {
        int firstNumber = 0;
        int secondNumber = 1;
        int fibonacciNumber = 0;

        while (fibonacciNumber < number) {
            fibonacciNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = fibonacciNumber;
        }
        return number == fibonacciNumber;
    }

    public void validateUpdateTask(Task task, UpdateTaskRequest request) {
        if (request.points > task.getPoints()) {
            int pointsDifference = request.points - task.getPoints();
            if ((task.getAssignee().getAssignedPoints() + pointsDifference) > userMaxAssignedPoints) {
                throw new ValidationException("The new number of points would exceed the user limit of "
                        + userMaxAssignedPoints);
            }
        }
        if (!doesBelongToFibonacciSequence(request.points)) {
            throw new ValidationException("Number of points must belong to Fibonacci sequence");
        }
    }

    public void canAssignTask(Person person, Task task) {
        if ((person.getAssignedPoints() + task.getPoints()) > userMaxAssignedPoints) {
            throw new ValidationException("User cannot accept this many points");
        }
    }

    public void validateTaskStatus(String taskStatus) {
        try {
            Task.TaskStatus.valueOf(taskStatus);
        } catch (IllegalArgumentException e) {
            throw new ValidationException("No such task status");
        }
    }

    public int resolveRequiredAssignedPoints(Person person, Task task) {
        return (person.getAssignedPoints() - userMaxAssignedPoints) + task.getPoints();
    }
}
