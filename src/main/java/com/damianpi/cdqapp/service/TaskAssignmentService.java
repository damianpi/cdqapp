package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.TaskRepository;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import com.damianpi.cdqapp.service.exception.PersonException;
import com.damianpi.cdqapp.service.exception.TaskException;
import com.damianpi.cdqapp.service.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Service
public class TaskAssignmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskAssignmentService.class);

    private static final String NO_USER_TO_AUTOASSIGN = "Cannot find any user to autoassign";

    private final TaskRepository taskRepository;
    private final PersonRepository personRepository;
    private final TaskValidator taskValidator;

    public TaskAssignmentService(TaskRepository taskRepository,
                                 PersonRepository personRepository,
                                 TaskValidator taskValidator) {
        this.taskRepository = taskRepository;
        this.personRepository = personRepository;
        this.taskValidator = taskValidator;
    }

    @Transactional
    public void assignTaskWithPerson(String username, UUID taskUuid, boolean forceAssignment) {
        Task task = findTaskByUuid(taskUuid);
        Person person = personRepository.findByUsername(username);

        if (task.getAssignee() != null) {
            detachTaskFromPerson(task);
        }
        if (forceAssignment) {
            person = prepareToAcceptTask(person, task);
        }
        taskValidator.canAssignTask(person, task);
        assignTaskWithPerson(task, person);
        personRepository.save(person);
    }

    private Person prepareToAcceptTask(Person person, Task task) {
        try {
            taskValidator.canAssignTask(person, task);
            return person;
        } catch (ValidationException e) {
            LOGGER.info("User " + person.getUsername() + " does not have enough points. Attempting to reassign tasks.");
        }
        List<Task> tasksToReassign = resolveTasksToReassign(person, task);
        return reassignTasksToOtherThan(tasksToReassign, person);
    }

    private List<Task> resolveTasksToReassign(Person person, Task task) {
        int requiredPoints = taskValidator.resolveRequiredAssignedPoints(person, task);
        person.getTasks().sort(Comparator.comparing(Task::getPoints));
        List<Task> tasksToReassign = new ArrayList<>();
        int freedPoints = 0;

        while (freedPoints < requiredPoints) {
            Task taskToReassign = person.getTasks().stream()
                    .filter(t -> t.getTaskStatus() == Task.TaskStatus.OPEN)
                    .findFirst()
                    .orElseThrow(() -> new PersonException("Cannot force task assignment. " +
                            "User does not have enough open tasks to reassign."));
            freedPoints += taskToReassign.getPoints();
            tasksToReassign.add(taskToReassign);
            person.getTasks().remove(taskToReassign);
        }
        return tasksToReassign;
    }

    private Person reassignTasksToOtherThan(List<Task> tasksToReassign, Person person) {
        tasksToReassign.forEach(t -> {
            person.removeTask(t);
            autoassignTask(t.getUuid(), person);
        });
        person.setAssignedPoints(recalculatePoints(person));
        return personRepository.save(person);
    }

    private void assignTaskWithPerson(Task task, Person person) {
        person.addTask(task);
        task.setAssignee(person);
        person.setAssignedPoints(recalculatePoints(person));
        LOGGER.info("Task " + task.getUuid() + " assigned to user " + person.getUsername());
    }

    public void autoassignTask(UUID taskUuid, Person avoidPerson) {
        Person person = resolveAutoassignPerson(avoidPerson);
        Task task = findTaskByUuid(taskUuid);

        try {
            taskValidator.canAssignTask(person, task);
        } catch (ValidationException e) {
            LOGGER.info("Cannot autoassign task uuid: " + task.getUuid().toString()
                    + " The person with the lowest number of active points cannot accept another assignment.");
            detachForcedReassignedTask(task);
            return;
        }
        assignTaskWithPerson(task, person);
        personRepository.save(person);
        LOGGER.info("Task " + task.getUuid() + " has been assigned to user " + person.getUsername());
    }

    private void detachForcedReassignedTask(Task task) {
        if (task.getAssignee() != null) {
            LOGGER.info("Detaching task " + task.getUuid() + " from user " + task.getAssignee().getUsername());
            task.setAssignee(null);
            taskRepository.save(task);
        }
    }

    private Person resolveAutoassignPerson(Person avoidPerson) {
        if (avoidPerson == null) {
            return personRepository.findFirstByOrderByAssignedPointsAsc()
                    .orElseThrow(() -> new PersonException(NO_USER_TO_AUTOASSIGN));
        } else {
            return personRepository.findFirstByUsernameNotLikeOrderByAssignedPointsAsc(avoidPerson.getUsername())
                    .orElseThrow(() -> new PersonException(NO_USER_TO_AUTOASSIGN));
        }
    }

    public int recalculatePoints(Person person) {
        return person.getTasks()
                .stream()
                .filter(t -> t.getTaskStatus() != Task.TaskStatus.CLOSED)
                .map(Task::getPoints)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public void detachTaskFromPerson(Task task) {
        Person person = task.getAssignee();
        task.setAssignee(null);
        person.removeTask(task);
        person.setAssignedPoints(recalculatePoints(person));
        personRepository.save(person);
    }

    private Task findTaskByUuid(UUID uuid) {
        return taskRepository.findById(uuid)
                .orElseThrow(() -> new TaskException("Task uuid " + uuid.toString() + " does not exist." ));
    }
}
