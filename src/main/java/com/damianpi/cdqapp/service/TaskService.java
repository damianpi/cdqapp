package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.*;
import com.damianpi.cdqapp.repository.PersonRepository;
import com.damianpi.cdqapp.repository.TaskRepository;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import com.damianpi.cdqapp.service.exception.TaskException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TaskService {

    private static final String NO_SUCH_TASK = "Task with the given UUID does not exist";

    private final TaskRepository taskRepository;
    private final TaskValidator taskValidator;
    private final TaskAssignmentService taskAssignmentService;
    private final PersonRepository personRepository;

    public TaskService(TaskRepository taskRepository,
                       TaskValidator taskValidator,
                       TaskAssignmentService taskAssignmentService,
                       PersonRepository personRepository) {
        this.taskRepository = taskRepository;
        this.taskValidator = taskValidator;
        this.taskAssignmentService = taskAssignmentService;
        this.personRepository = personRepository;
    }

    @Transactional
    public UUID createTask(TaskRequest request) {
        taskValidator.validateNewTask(request);
        Task task = taskRepository.save(TaskFactory.of(request));
        taskAssignmentService.autoassignTask(task.getUuid(), null);
        return task.getUuid();
    }

    public TaskResponse getTask(UUID uuid) {
        return TaskFactory.of(taskRepository.findById(uuid)
                .orElseThrow(() -> new TaskException(NO_SUCH_TASK)));
    }

    @Transactional
    public void updateTask(UpdateTaskRequest request) {
        Task task = findTaskByUuid(request.uuid);
        taskValidator.validateUpdateTask(task, request);
        Person assignee = task.getAssignee();
        TaskFactory.update(assignee.getTaskByUuid(task.getUuid())
                .orElseThrow(() -> new TaskException(NO_SUCH_TASK)), request);
        assignee.setAssignedPoints(taskAssignmentService.recalculatePoints(assignee));
        personRepository.save(assignee);
    }

    @Transactional
    public void deleteTask(UUID uuid) {
        Task task = findTaskByUuid(uuid);
        taskRepository.delete(task);
        Person assignee = task.getAssignee();
        assignee.removeTask(task);
        assignee.setAssignedPoints(taskAssignmentService.recalculatePoints(assignee));
        personRepository.save(assignee);
    }

    public List<TaskSummaryDTO> listTasks(String username, String taskStatus) {
        taskValidator.validateTaskStatus(taskStatus);

        return personRepository.findByUsername(username)
                .getTasks()
                .stream()
                .filter(t -> t.getTaskStatus() == Task.TaskStatus.valueOf(taskStatus))
                .map(ft -> {
                    TaskSummaryDTO dto = new TaskSummaryDTO();
                    dto.title = ft.getTitle();
                    dto.uuid = ft.getUuid();
                    dto.points = ft.getPoints();
                    dto.taskStatus = ft.getTaskStatus().toString();
                    return dto;
                })
                .collect(Collectors.toList());
    }

    public List<TaskSummaryDTO> listAssignedTasks() {
        return taskRepository.findAllActiveTasks()
                .stream()
                .map(ft -> {
                    TaskSummaryDTO dto = new TaskSummaryDTO();
                    dto.title = ft.getTitle();
                    dto.uuid = ft.getUuid();
                    dto.points = ft.getPoints();
                    dto.taskStatus = ft.getTaskStatus().toString();
                    dto.assignee = ft.getAssignee().getUsername();
                    return dto;
                }).collect(Collectors.toList());
    }

    public void startProgress(UUID uuid) {
        Task task = findTaskByUuid(uuid);
        if (task.getTaskStatus() != Task.TaskStatus.OPEN) {
            throw new TaskException("Cannot start progress for task "
                    + uuid + ". Current status is " + task.getTaskStatus().toString());
        }
        task.setTaskStatus(Task.TaskStatus.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Transactional
    public void stopProgress(UUID uuid) {
        Person person = findTaskByUuid(uuid).getAssignee();
        Task task = person.getTasks()
                .stream()
                .filter(t -> t.getUuid().equals(uuid))
                .findFirst()
                .orElseThrow(() -> new TaskException(NO_SUCH_TASK));
        if (task.getTaskStatus() != Task.TaskStatus.IN_PROGRESS) {
            throw new TaskException("Cannot close task that is not in progress: " + uuid);
        }
        task.setTaskStatus(Task.TaskStatus.CLOSED);
        person.setAssignedPoints(taskAssignmentService.recalculatePoints(person));
        personRepository.save(person);
    }

    private Task findTaskByUuid(UUID uuid) {
        return taskRepository.findById(uuid).orElseThrow(() -> new TaskException(NO_SUCH_TASK));
    }
}
