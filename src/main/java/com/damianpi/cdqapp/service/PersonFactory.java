package com.damianpi.cdqapp.service;

import com.damianpi.cdqapp.controller.dto.PersonRequest;
import com.damianpi.cdqapp.controller.dto.PersonResponse;
import com.damianpi.cdqapp.controller.dto.TaskSummaryDTO;
import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.PersonDetails;
import com.damianpi.cdqapp.repository.entity.Task;
import java.util.stream.Collectors;

public class PersonFactory {

    public static Person of(PersonRequest request) {
        Person person = new Person();
        person.setUsername(request.username);
        person.setPersonDetails(new PersonDetails(request.name, request.surname, request.position));
        return person;
    }

    public static PersonResponse of(Person person) {
        PersonResponse personResponse = new PersonResponse();
        personResponse.username = person.getUsername();
        personResponse.name = person.getPersonDetails().getName();
        personResponse.surname = person.getPersonDetails().getSurname();
        personResponse.position = person.getPersonDetails().getPosition();
        personResponse.tasks = person.getTasks().stream()
                .filter(e -> e.getTaskStatus() != Task.TaskStatus.CLOSED)
                .map(e -> {
                    TaskSummaryDTO dto = new TaskSummaryDTO();
                    dto.uuid = e.getUuid();
                    dto.title = e.getTitle();
                    dto.points = e.getPoints();
                    dto.taskStatus = e.getTaskStatus().toString();
                    return dto; })
                .collect(Collectors.toList());
        return personResponse;
    }
}
