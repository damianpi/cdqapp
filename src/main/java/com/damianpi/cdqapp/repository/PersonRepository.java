package com.damianpi.cdqapp.repository;

import com.damianpi.cdqapp.repository.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByUsername(String username);
    void deleteByUsername(String username);
    Optional<Person> findFirstByOrderByAssignedPointsAsc();
    Optional<Person> findFirstByUsernameNotLikeOrderByAssignedPointsAsc(String username);
}
