package com.damianpi.cdqapp.repository.entity;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 4)
    @Column(unique = true)
    private String username;

    @OneToMany(mappedBy = "assignee", cascade = {CascadeType.DETACH, CascadeType.PERSIST}, fetch = FetchType.EAGER)
    private List<Task> tasks = new ArrayList<>();

    private int assignedPoints;

    @Embedded
    private PersonDetails personDetails;

    public Person() {
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
    }

    public Optional<Task> getTaskByUuid(UUID uuid) {
        return tasks.stream().filter(t -> t.getUuid().equals(uuid)).findFirst();
    }

    public Long getId() {
        return id;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public PersonDetails getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(PersonDetails personDetails) {
        this.personDetails = personDetails;
    }

    public Integer getAssignedPoints() {
        return assignedPoints;
    }

    public void setAssignedPoints(Integer assignedPoints) {
        this.assignedPoints = assignedPoints;
    }
}
