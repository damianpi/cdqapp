package com.damianpi.cdqapp.repository.entity;

import javax.persistence.Embeddable;

@Embeddable
public class PersonDetails {
    private String name;
    private String surname;
    private String position;

    public PersonDetails() {
    }

    public PersonDetails(String name, String surname, String position) {
        this.name = name;
        this.surname = surname;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
