package com.damianpi.cdqapp.repository;

import com.damianpi.cdqapp.repository.entity.Person;
import com.damianpi.cdqapp.repository.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface TaskRepository extends JpaRepository<Task, UUID> {

    @Query("from Task t where t.assignee = :person and t.taskStatus != 'CLOSED'")
    Stream<Task> findActiveTasksToRecalculate(Person person);

    @Query("from Task t where t.assignee != null and t.taskStatus != 'CLOSED'")
    List<Task> findAllActiveTasks();
}
